//
//  GithubService.swift
//  hubhub
//
//  Created by Ярослав Попов on 27/05/2019.
//  Copyright © 2019 Ярослав Попов. All rights reserved.
//

import Foundation
import UIKit.UIImage

final class GithubService {
    
    let provider = NetworkingProvider<GithubTarget>()
    let resourceProvider = NetworkingProvider<GithubResourcesTarget>()
    
    func searchRepositories(by query: String, page: Int, completion: @escaping ServiceCompletion<GithubSearchRepositoryResponse>) {
        
        provider.callAPI(target: .search(query: query, page: page)) { response in
            switch response {
            case .success(let result):
                if let githubResponse = try? JSONDecoder().decode(GithubSearchRepositoryResponse.self, from: result.data) {
                completion(.success(githubResponse))
            } else {
                completion(.failure(.apiError))
            }
            case .failure:
            completion(.failure(.apiError))
        }
    }
}
    
    func searchIssues(by query: String?, fullName: String, page: Int, completion: @escaping ServiceCompletion<GithubSearchIssue>) {
        provider.callAPI(target: .issues(query: query ?? "", repo: fullName, page: page)) { response in
            switch response {
            case .success(let result):
                if let listIssues = try? JSONDecoder().decode(GithubSearchIssue.self, from: result.data) {
                    completion(.success(listIssues))
                } else {
                    completion(.failure(.apiError))
                }
            case .failure:
                completion(.failure(.apiError))
            }
        }
    }
    
    func loadImage(by url: String, completion: @escaping ServiceCompletion<UIImage>) {
        resourceProvider.callAPI(target: .photo(url: url)) { response in
            switch response {
            case .success(let result):
                if let image = UIImage(data: result.data) {
                    completion(.success(image))
                } else {
                    completion(.failure(.apiError))
                }
            case .failure:
                completion(.failure(.apiError))
            }
        }
    }
}
