//
//  IssueCell.swift
//  hubhub
//
//  Created by Ярослав Попов on 27/05/2019.
//  Copyright © 2019 Ярослав Попов. All rights reserved.
//

import Foundation
import UIKit

class IssueCell: UITableViewCell {
    
    @IBOutlet weak var title: UILabel!
    @IBOutlet weak var statusImage: UIImageView!
    @IBOutlet weak var commentsCount: UILabel!
    @IBOutlet weak var bodyPreview: UILabel!
    
    required init?(coder aDecoder: NSCoder) {
        super.init(coder: aDecoder)
    }
    
    override init(style: UITableViewCell.CellStyle, reuseIdentifier: String?) {
        super.init(style: style, reuseIdentifier: reuseIdentifier)
    }
    
    func configure(issue: GithubIssue) {
        let imageName = "Issue\(issue.state)"
        let isOpen = issue.state == "open"
        statusImage.image = UIImage(named: imageName)
        statusImage.tintColor = isOpen ? .open : .closed
        commentsCount.text = String(issue.comments)
        bodyPreview.text = issue.body
        
        let number = "(#\(issue.number))"
        let string = NSMutableAttributedString(string: "\(issue.title) \(number)")
        string.setColor(for: number, with: .blue)
       
        title.attributedText = string
    }
}
