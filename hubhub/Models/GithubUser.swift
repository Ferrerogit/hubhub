//
//  GithubUser.swift
//  hubhub
//
//  Created by Ярослав Попов on 28/05/2019.
//  Copyright © 2019 Ярослав Попов. All rights reserved.
//

import Foundation

struct GithubUser: Decodable {
    let login: String
    let avatarUrl: String
    
    enum CodingKeys: String, CodingKey {
        case login
        case avatarUrl = "avatar_url"
    }
}
