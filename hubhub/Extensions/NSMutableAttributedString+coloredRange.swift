//
//  NSMutableAttributedString+coloredRange.swift
//  hubhub
//
//  Created by Ярослав Попов on 27/05/2019.
//  Copyright © 2019 Ярослав Попов. All rights reserved.
//

import Foundation
import UIKit.UIColor
extension NSMutableAttributedString {
    
    private func detectRange(for text: String?) -> NSRange {
        guard let text = text else {
            return NSMakeRange(0, self.length)
        }
         return self.mutableString.range(of: text, options: .caseInsensitive)
    }
    
    func setColor(for text: String?, with color: UIColor) {
        let range = detectRange(for: text)
        
        if range.location != NSNotFound {
            addAttribute(NSAttributedString.Key.foregroundColor, value: color, range: range)
        }
    }
    
    func setRoundedBackground(for text: String?, with color: UIColor) {
        let range = detectRange(for: text)
        
        if range.location != NSNotFound {
            addAttribute(NSAttributedString.Key.backgroundColor, value: color, range: range)
        }
    }
    
    func setSemiBoldWeight(for text: String?, font: UIFont) {
        let range = detectRange(for: text)
        let initialFont = font
        let newFont = UIFont(name: font.fontName+"-SemiBold", size: initialFont.pointSize)
        if range.location != NSNotFound {
            addAttribute(NSAttributedString.Key.font, value: newFont!, range: range)
        }
    }
}
