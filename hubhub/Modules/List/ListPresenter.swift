//
//  ListPresenter.swift
//  hubhub
//
//  Created by Ярослав Попов on 27/05/2019.
//  Copyright © 2019 Ярослав Попов. All rights reserved.
//

import Foundation

protocol ListPresentation: class {
    func viewDidLoad()
    func viewWillAppear()
    
    func onLoadedCell(with index: Int)
    func onSearchQueryChanged(newQuery: String?)
    
    func didSelectCell(with index: Int)
    func didBeginRefresh()
}

protocol ListView: class {
    var presenter: ListPresentation!  { get set }
    
    func open(repository: GithubRepository)
    func set(repositoriesList: [GithubRepository])
}

final class ListPresenter: ListPresentation {
    func didBeginRefresh() {
        refresh()
    }
    
    func didSelectCell(with index: Int) {
        self.view?.open(repository: repositories[index])
    }
    
    func onLoadedCell(with index: Int) {
        guard index > (currentPage * 30) / 4 * 3 else {return}
        loadMore() { [weak self] list in
            guard let self = self else {return}
            self.totalCount = list.total
            self.repositories += list.items
        }
    }
    
    private func refresh() {
        currentPage = 0
        loadMore { [weak self] response in
            guard let self = self else {return}
            self.totalCount = response.total
            self.repositories = response.items
        }
    }
    
    func onSearchQueryChanged(newQuery: String?) {
        guard let query = newQuery, query.count > 3 else {return}
        currentQuery = query
        refresh()
    }
    
    func viewDidLoad() {
        refresh()
    }
    
    func viewWillAppear() {
    }
    
    private func loadMore(onSuccess: @escaping (GithubSearchRepositoryResponse) -> Void) {
        currentPage += 1
        ServicesManager.github.searchRepositories(by: currentQuery, page: currentPage) { result in
            switch result {
            case .success(let list):
                onSuccess(list)
            case .failure:
                print("failure")
            }
        }
    }
    
    
    
    var currentQuery = "Swift"
    var currentPage = 0
    weak var view: ListView?
    var totalCount: Int = 0
    var repositories = [GithubRepository]() {
        didSet {
            self.view?.set(repositoriesList: repositories)
        }
    }
    
    init(view: ListView) {
        self.view = view
    }
}
