//
//  GithubSearchIssue.swift
//  hubhub
//
//  Created by Ярослав Попов on 27/05/2019.
//  Copyright © 2019 Ярослав Попов. All rights reserved.
//

import Foundation

struct GithubSearchIssue: Decodable {
    enum CodingKeys: String, CodingKey {
        case total = "total_count"
        case items
    }
    
    let total: Int
    let items: [GithubIssue]
}
