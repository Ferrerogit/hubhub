//
//  GithubResourcesTarget.swift
//  hubhub
//
//  Created by Ярослав Попов on 28/05/2019.
//  Copyright © 2019 Ярослав Попов. All rights reserved.
//

import Foundation
enum GithubResourcesTarget: Target {
    case photo(url: String)
 
    
    var parameters: [String : Any] {
        return [:]
    }
    
    var baseURL: String {
        return ""
    }
    
    var path: String {
        switch self {
        case .photo(let url):
            return url
        }
    }
    
    var method: RequestMethod {
        return .get
    }
    
    var headers: [String : String]? {
        switch self {
        default: return nil
        }
    }
}
