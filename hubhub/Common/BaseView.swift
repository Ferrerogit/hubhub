//
//  BaseView.swift
//  hubhub
//
//  Created by Ярослав Попов on 27/05/2019.
//  Copyright © 2019 Ярослав Попов. All rights reserved.
//

import Foundation
import UIKit

protocol BaseView: class {
    func presentAlert(title: String?, message: String, handler: ((UIAlertAction) -> Void)?)
    func presentErrorAlert(message: String)
    func dismiss(animated: Bool, completion: (() -> Void)?)
    func popFromNavigationController(animated: Bool)
    func popToRootViewController(animated: Bool)
    func present(_ viewController: UIViewController, animated: Bool)
    func pushViewController(newViewController: UIViewController)
}

extension UIViewController: BaseView {
    
    func presentAlert(title: String?, message: String, handler: ((UIAlertAction) -> Void)? = nil) {
        let alert = UIAlertController(title: title, message: message, preferredStyle: .alert)
        let action = UIAlertAction(title: "Закрыть", style: .cancel, handler: handler)
        alert.addAction(action)
        DispatchQueue.main.async {
            self.present(alert, animated: true, completion: nil)
        }
    }
    
    func presentErrorAlert(message: String) {
        let alert = UIAlertController(title: "Ошибка!", message: message, preferredStyle: .alert)
        let action = UIAlertAction(title: "Закрыть", style: .default, handler: nil)
        alert.addAction(action)
        DispatchQueue.main.async {
            self.present(alert, animated: true, completion: nil)
        }
    }
    
    func popFromNavigationController(animated: Bool = true) {
        DispatchQueue.main.async {
            let navigationVC = self.navigationController ?? self as? UINavigationController
            navigationVC?.popViewController(animated: true)
        }
    }
    
    func popToRootViewController(animated: Bool) {
        DispatchQueue.main.async {
            let navigationVC = self.navigationController ?? self as? UINavigationController
            navigationVC?.popToRootViewController(animated: true)
        }
    }
    
    func pushViewController(newViewController: UIViewController) {
        DispatchQueue.main.async {
            let navigationVC = self.navigationController ?? self as? UINavigationController
            navigationVC?.pushViewController(newViewController, animated: true)
        }
    }
    
    
    func present(_ viewController: UIViewController, animated: Bool = true) {
        DispatchQueue.main.async {
            self.present(viewController, animated: animated, completion: nil)
        }
    }
}
