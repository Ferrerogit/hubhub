//
//  IssuePresenter.swift
//  hubhub
//
//  Created by Ярослав Попов on 27/05/2019.
//  Copyright © 2019 Ярослав Попов. All rights reserved.
//

import UIKit.UIImage
import Foundation
protocol IssuePresentation: class {
    func viewDidLoad()
    func viewWillAppear()
}

protocol IssueView: class {
    var presenter: IssuePresentation!  { get set }
    
    func set(issue: GithubIssue)
    func onAvatarLoaded(image: UIImage)
}

final class IssuePresenter: IssuePresentation {
    
    func viewDidLoad() {
        self.view?.set(issue: issue)
        loadAvatar()
    }
    
    private func loadAvatar() {
        ServicesManager.github.loadImage(by: issue.user.avatarUrl) { result in
            switch result {
            case .success(let image):
                self.view?.onAvatarLoaded(image: image)
            case .failure(_):
                break
            }
        }
    }
    
    func viewWillAppear() {
    }

    private var issue: GithubIssue {
        didSet {
            self.view?.set(issue: issue)
        }
    }
    
    weak var view: IssueView?
   
    init(view: IssueView, issue: GithubIssue) {
        self.issue = issue
        self.view = view
    }
}
