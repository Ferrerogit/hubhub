//
//  RepositoryViewController.swift
//  hubhub
//
//  Created by Ярослав Попов on 27/05/2019.
//  Copyright © 2019 Ярослав Попов. All rights reserved.
//

import Foundation
import UIKit

class IssuesViewController: UITableViewController {
    
    let searchController = UISearchController(searchResultsController: nil)
    let refreshController = UIRefreshControl()
    
    private var issues: [GithubIssue] = [GithubIssue]() {
        didSet {
            tableView.reloadData()
            refreshController.endRefreshing()
        }
    }
    
    private var searchBarDebounce: DispatchWorkItem?
    
    var presenter: IssuesPresentation!
    
    override func viewDidLoad() {
        super.viewDidLoad()
        setup()
        presenter.viewDidLoad()
    }
    
    
    private func setup() {
        searchController.searchBar.placeholder = " Search issues..."
        searchController.searchBar.sizeToFit()
        searchController.searchBar.isTranslucent = false
        searchController.searchBar.backgroundImage = UIImage()
        searchController.searchBar.barTintColor = .white
        navigationItem.title = "Search issues"
        tableView.refreshControl = refreshController
        refreshController.addTarget(self, action: #selector(refresh(_:)), for: .valueChanged)
        searchController.searchBar.delegate = self
        searchController.delegate = self
        searchController.searchBar.tintColor = .white
        searchController.searchBar.barStyle = .black
        
        self.searchController.dimsBackgroundDuringPresentation = false
        navigationItem.searchController = searchController
        navigationItem.hidesSearchBarWhenScrolling = false
        
        tableView.register(UINib(nibName: "IssueCell", bundle: nil), forCellReuseIdentifier: "issueCell")
        tableView.rowHeight = 100.0
    }
    
    @objc private func refresh(_ sender: Any) {
        self.presenter.didBeginRefresh()
    }
    
    override func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        self.presenter.didSelectCell(with: indexPath.row)
    }
    
    override func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return issues.count
    }
    
    override func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        guard let cell = tableView.dequeueReusableCell(withIdentifier: "issueCell", for: indexPath) as? IssueCell else {
            return UITableViewCell()
        }
        
        cell.configure(issue: issues[indexPath.row])
        presenter.onLoadedCell(with: indexPath.row)
        return cell
    }
    
}

extension IssuesViewController: IssuesView {
    func set(issues: [GithubIssue]) {
        self.issues = issues
    }
    
    func open(issue: GithubIssue) {
        searchController.isActive = false
        let vc = UIStoryboard(name: "Main", bundle: nil).instantiateViewController(withIdentifier: String(describing: IssueViewController.self)) as! IssueViewController
        
        let presenter = IssuePresenter(view: vc, issue: issue)
        
        vc.presenter = presenter
        self.navigationController?.pushViewController(vc, animated: true)
    }
}

extension IssuesViewController: UISearchControllerDelegate, UISearchBarDelegate, UISearchDisplayDelegate {
    
    func searchBar(_ searchBar: UISearchBar, textDidChange searchText: String) {
        searchBarDebounce?.cancel()
        searchBarDebounce = DispatchWorkItem {
            self.presenter.onSearchQueryChanged(newQuery: searchText)
        }
        DispatchQueue.main.asyncAfter(deadline: .now() + 0.3, execute: searchBarDebounce!)
    }
    
    func searchBar(_ searchBar: UISearchBar, shouldChangeTextIn range: NSRange, replacementText text: String) -> Bool {
        return text.isEmpty || text.matches("^[a-zA-Z0-9$@$!%*?&#^-_. +]+$")
    }
    
    func searchBarShouldBeginEditing(_ searchBar: UISearchBar) -> Bool {
        tableView.refreshControl = nil
        return true
    }
    
    func searchBarShouldEndEditing(_ searchBar: UISearchBar) -> Bool {
        tableView.refreshControl = refreshController
        return true
    }
}
