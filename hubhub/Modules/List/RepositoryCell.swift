//
//  RepositoryCel.swift
//  hubhub
//
//  Created by Ярослав Попов on 27/05/2019.
//  Copyright © 2019 Ярослав Попов. All rights reserved.
//

import Foundation
import UIKit

class RepositoryCell: UITableViewCell {
    
    @IBOutlet weak var title: UILabel!
    @IBOutlet weak var forksCounter: UILabel!
    @IBOutlet weak var watchesCounter: UILabel!
    @IBOutlet weak var starsCounter: UILabel!
    @IBOutlet weak var issuesCounter: UILabel!
    
    required init?(coder aDecoder: NSCoder) {
        super.init(coder: aDecoder)
    }
    
    override init(style: UITableViewCell.CellStyle, reuseIdentifier: String?) {
        super.init(style: style, reuseIdentifier: reuseIdentifier)
    }
    
    func configure(repository: GithubRepository) {
        title.text = repository.name
        forksCounter.text = String(repository.forks)
        watchesCounter.text = String(repository.watchers)
        starsCounter.text = String(repository.stars)
        issuesCounter.text = String(repository.issues)
    }
}
