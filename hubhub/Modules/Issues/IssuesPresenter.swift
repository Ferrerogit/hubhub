//
//  RepositoryPresenter.swift
//  hubhub
//
//  Created by Ярослав Попов on 27/05/2019.
//  Copyright © 2019 Ярослав Попов. All rights reserved.
//

import Foundation

protocol IssuesPresentation: class {
    func viewDidLoad()
    func viewWillAppear()
    func onLoadedCell(with index: Int)
    func onSearchQueryChanged(newQuery: String?)
    
    func didBeginRefresh()
    func didSelectCell(with index: Int)
}

protocol IssuesView: class {
    var presenter: IssuesPresentation!  { get set }
    
    func set(issues: [GithubIssue])
    
    func open(issue: GithubIssue)
}

final class IssuesPresenter: IssuesPresentation {
    func didSelectCell(with index: Int) {
        self.view?.open(issue: issues[index])
    }
    
    func didBeginRefresh() {
        refresh()
    }
    
    func viewDidLoad() {
        refresh()
    }
    
    func viewWillAppear() {
    }
    
    func onLoadedCell(with index: Int) {
        guard index > (currentPage * 30) / 4 * 3 else {return}
        loadMore { response in
            self.totalCount = response.total
            self.issues += response.items
        }
    }
    
    func onSearchQueryChanged(newQuery: String?) {
        guard let query = newQuery, query.count > 3 else {return}
        currentQuery = query
        refresh()
    }
    
    private func refresh() {
        currentPage = 0
        loadMore { response in
            self.totalCount = response.total
            self.issues = response.items
        }
    }
    
    private func loadMore(onSuccess: @escaping (GithubSearchIssue) -> Void) {
        currentPage += 1
        ServicesManager.github.searchIssues(by: currentQuery, fullName: repository.fullName, page: currentPage) { [weak self] result in
            guard self != nil else {return}
            switch result {
            case .success(let list):
                onSuccess(list)
            case .failure:
                print("failure")
            }
        }
    }
    
    private var currentQuery: String?
    private var currentPage: Int = 0
    private var totalCount = 0
    private var issues = [GithubIssue]() {
        didSet {
            self.view?.set(issues: issues)
        }
    }
    
    weak var view: IssuesView?
    let repository: GithubRepository
    init(view: IssuesView, repository: GithubRepository) {
        self.view = view
        self.repository = repository
    }
    
    
}
