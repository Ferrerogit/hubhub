//
//  UIColor+brand.swift
//  hubhub
//
//  Created by Ярослав Попов on 27/05/2019.
//  Copyright © 2019 Ярослав Попов. All rights reserved.
//

import UIKit

extension UIColor {
    static var brand: UIColor {
        return UIColor(red: 36 / 255, green: 42 / 255, blue: 46 / 255, alpha: 1)
    }
    
    static var closed: UIColor {
        return UIColor(red: 203 / 255, green: 36 / 255, blue: 49 / 255, alpha: 1)
    }
    
    static var open: UIColor {
        return UIColor(red: 40 / 255, green: 167 / 255, blue: 69 / 255, alpha: 1)
    }
    
    static func from(hex: String) -> UIColor {
        var cString:String = hex.trimmingCharacters(in: .whitespacesAndNewlines).uppercased()
        
        if (cString.hasPrefix("#")) {
            cString.remove(at: cString.startIndex)
        }
        
        if ((cString.count) != 6) {
            return UIColor.gray
        }
        
        var rgbValue:UInt32 = 0
        Scanner(string: cString).scanHexInt32(&rgbValue)
        
        return UIColor(
            red: CGFloat((rgbValue & 0xFF0000) >> 16) / 255.0,
            green: CGFloat((rgbValue & 0x00FF00) >> 8) / 255.0,
            blue: CGFloat(rgbValue & 0x0000FF) / 255.0,
            alpha: CGFloat(1.0)
        )
    }
}
