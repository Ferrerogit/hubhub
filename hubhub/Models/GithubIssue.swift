//
//  GithubIssue.swift
//  hubhub
//
//  Created by Ярослав Попов on 27/05/2019.
//  Copyright © 2019 Ярослав Попов. All rights reserved.
//

import Foundation

struct GithubIssue: Decodable {
    let id: Int
    let number: Int
    let state: String
    let title: String
    let body: String
    let labels: [GithubIssueLabel]
    let comments: Int
    
    let user: GithubUser
    let createdAt: String
    
    enum CodingKeys: String, CodingKey {
        case id
        case number
        case state
        case title
        case body
        case labels
        case comments
        case user
        case createdAt = "created_at"
    }
}
