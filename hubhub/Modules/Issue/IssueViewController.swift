//
//  IssueViewController.swift
//  hubhub
//
//  Created by Ярослав Попов on 27/05/2019.
//  Copyright © 2019 Ярослав Попов. All rights reserved.
//

import Foundation
import UIKit

class IssueViewController: UIViewController {

    @IBOutlet weak var statusBackground: UIView!
    @IBOutlet weak var statusIcon: UIImageView!
    @IBOutlet weak var statusLabel: UILabel!
    @IBOutlet weak var userLabel: UILabel!
    @IBOutlet weak var issueTitle: UILabel!
    @IBOutlet weak var nameInBodyHeaderLabel: UILabel!
    @IBOutlet weak var issueBodyTextView: UITextView!
    
    @IBOutlet weak var userAvatar: UIImageView!
    
    @IBOutlet weak var bodyContrainer: UIView!
    
    var presenter: IssuePresentation!
    var issue: GithubIssue! {
        didSet {
            configure()
        }
    }
    var inputDateFormatter: DateFormatter {
        let dateFormatter = DateFormatter()
        dateFormatter.dateFormat = "yyyy-MM-dd'T'HH:mm:ssZZZZZ"
        dateFormatter.locale = Locale(identifier: "en_US_POSIX")
        return dateFormatter
    }
    
    var outputDateFormatter: DateFormatter {
        let dateFormatter = DateFormatter()
        
        dateFormatter.dateFormat = "dd MMM yyyy"
        dateFormatter.locale = Locale(identifier: "en_US_POSIX")
        return dateFormatter
    }
    
    override func viewDidLoad() {
        super.viewDidLoad()
        setup()
        presenter.viewDidLoad()
    }
    
    private func setup() {
        statusBackground.layer.cornerRadius = 3
    }
    
    func configure() {
        let number = "(#\(issue.number))"
        var string = "\(issue.title) \(number)"
        
        let isOpen = issue.state == "open"
        
        statusBackground.backgroundColor = isOpen ? .open : .closed
        statusIcon.image = UIImage(named: "Issue\(issue.state)")?.withRenderingMode(.alwaysTemplate)
        statusIcon.tintColor = .white
        statusLabel.text = issue.state.capitalized
        
        for label in issue.labels {
            string += "   \(label.name)  "
        }
        
        let issueLabelMutableString = NSMutableAttributedString(string: string)
        issueLabelMutableString.setColor(for: number, with: .blue)
        
        for label in issue.labels {
            let nameWithSpace = "  \(label.name)  "
            issueLabelMutableString.setRoundedBackground(for: nameWithSpace, with: UIColor.from(hex: label.color))
            issueLabelMutableString.setColor(for: nameWithSpace, with: .white)
        }
        issueTitle.attributedText = issueLabelMutableString
        
        let date = inputDateFormatter.date(from: issue.createdAt)! // '!' Because i trust github
        let dateString = outputDateFormatter.string(from: date)
        let issueCreator = "\(issue.user.login) opened this issue on \(dateString)"
        
        let issueCreatorMutableString = NSMutableAttributedString(string: issueCreator)
        
        issueCreatorMutableString.setSemiBoldWeight(for: issue.user.login, font: userLabel.font)
        issueCreatorMutableString.setSemiBoldWeight(for: dateString, font: userLabel.font)
        userLabel.attributedText = issueCreatorMutableString
        
        issueBodyTextView.sizeToFit()
        issueBodyTextView.text = issue.body
        issueBodyTextView.sizeToFit()

        nameInBodyHeaderLabel.text = issue.user.login
        view.layoutIfNeeded()
    }
}

extension IssueViewController: IssueView {
    func onAvatarLoaded(image: UIImage) {
        self.userAvatar.image = image
    }
    
    func set(issue: GithubIssue) {
        self.issue = issue
    }
}
