//
//  APITarget.swift
//  hubhub
//
//  Created by Ярослав Попов on 26/05/2019.
//  Copyright © 2019 Ярослав Попов. All rights reserved.
//

import Foundation

public enum RequestMethod: String {
    case get = "GET"
}

public protocol Target {
    
    var baseURL: String {get}
    
    var path: String {get}
    
    var method: RequestMethod {get}
    
    var headers: [String: String]? {get}
    
    var parameters: [String: Any] {get}
}
