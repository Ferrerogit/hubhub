//
//  NetworkingProvider.swift
//  hubhub
//
//  Created by Ярослав Попов on 27/05/2019.
//  Copyright © 2019 Ярослав Попов. All rights reserved.
//

import Foundation

final class NetworkingProvider<T: Target> {
    typealias NetworkingResult = Result<NetworkingResponse, DataError>
    typealias NetworkingCompletion = (_ result: NetworkingResult) -> Void
    
    func callAPI(target: T, completion: @escaping NetworkingCompletion) {
        let request = prepareRequest(for: target)
        URLSession.shared.dataTask(with: request) { data, response, error in
            DispatchQueue.main.async {
                guard error == nil else {
                    completion(.failure(.apiError))
                    return
                }
                
                if let response = response, let data = data {
                    completion(.success(NetworkingResponse(response: response, data: data)))
                } else {
                    completion(.failure(.apiError))
                }
            }
        }.resume()
    }
    
    private func prepareRequest(for target: T) -> URLRequest {
        
        var request = URLRequest(url: URL(string: (target.baseURL + target.path).addingPercentEncoding(withAllowedCharacters: .urlQueryAllowed)!)!)
        
        request.allHTTPHeaderFields = target.headers
        
        return request
    }
}
