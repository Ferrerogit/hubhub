//
//  APIResponse.swift
//  hubhub
//
//  Created by Ярослав Попов on 26/05/2019.
//  Copyright © 2019 Ярослав Попов. All rights reserved.
//

import Foundation
final class APIResponse {
    private let response: URLResponse?
    private(set) var serializedData: Any?
    
    var data: Any? {
        return nil
    }
    
    init(_ response: URLResponse, data: Data) {
        self.response = response
        serializedData = String(data: data, encoding: .utf8)
    }
    
    var error: DataError? {
        do {
            guard response != nil else {return .noData}
            _ = try filter()
        } catch {
            return .apiError
        }
        return nil
    }
    
    var statusCode: Int {
        return (response as! HTTPURLResponse).statusCode
    }
    
    private func filter() throws {
        guard (200...299).contains(statusCode) else {
            throw DataError.apiError
        }
    }
}
