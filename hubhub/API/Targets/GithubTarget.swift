//
//  GithubTarget.swift
//  hubhub
//
//  Created by Ярослав Попов on 26/05/2019.
//  Copyright © 2019 Ярослав Попов. All rights reserved.
//

import Foundation

enum GithubTarget: Target {
    case search(query: String, page: Int)
    case issues(query: String, repo: String, page: Int)
    
    var parameters: [String : Any] {
        return [:]
    }
    
    var baseURL: String {
        return "https://api.github.com/"
    }
    
    var path: String {
        var path = ""
        switch self {
        case .search(let query, let page):
            path += "search/repositories?q=\(query)&page=\(page)&per_page=30"
            path += "&client_id=639c4d99ebaad98149aa" +
            "&client_secret=727523ef75a007b93f904db87b4756e55a1f4b2a"
        case .issues(let q, let repo, let page):
            path += "search/issues?q=\(q)+repo:\(repo)&page=\(page)&per_page=30"
        }
        
        return path
    }
    
    var method: RequestMethod {
        return .get
    }
    
    var headers: [String : String]? {
        switch self {
        default: return nil
        }
    }
    
    
}
