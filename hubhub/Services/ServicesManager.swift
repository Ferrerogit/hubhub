//
//  ServicesManager.swift
//  hubhub
//
//  Created by Ярослав Попов on 27/05/2019.
//  Copyright © 2019 Ярослав Попов. All rights reserved.
//

import Foundation

typealias ServiceResult<Object> = Result<Object, DataError>
typealias ServiceCompletion<Object> = ((ServiceResult<Object>) -> Void)

final class ServicesManager {
    static let github = GithubService()
    private init() {}
}
