//
//  NetworkingResponse.swift
//  hubhub
//
//  Created by Ярослав Попов on 26/05/2019.
//  Copyright © 2019 Ярослав Попов. All rights reserved.
//

import Foundation

final class NetworkingResponse {
    private let response: URLResponse
    private(set) var data: Data
    
    init(response: URLResponse, data: Data) {
        self.response = response
        self.data = data
    }
}
