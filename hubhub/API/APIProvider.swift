//
//  APIProvider.swift
//  hubhub
//
//  Created by Ярослав Попов on 26/05/2019.
//  Copyright © 2019 Ярослав Попов. All rights reserved.
//

import Foundation


final class APIProvider {
    typealias APIResult = Result<APIResponse, DataError>
    typealias APICompletion = (_ result: APIResult) -> Void
    
    func callAPI(_ target: Target, completion: @escaping APIResult) {
        request(prepareRequest(for: target)) { result in
            switch result {
            case .success(let response):
                DispatchQueue.main.async {
                    response.serializedData
                }
            case .failure:
                break
//                DispatchQueue.main.async {
//                    completion(error)
//                }
            }
        }
    }
    
    private func request(_ request: URLRequest, completion: @escaping APICompletion) {
        URLSession.shared.dataTask(with: request) { data, response, error in
            guard let data = data, let response = response else {
                completion(.failure(.noData))
                return
            }
            
            if error != nil {
                completion(.failure(.apiError))
            }
            
            let apiResponse = APIResponse(response, data: data)
            completion(.success(apiResponse))
        }
    }
    
    private func prepareRequest(for target: Target) -> URLRequest {
        let url = URL(string: target.baseURL + target.path)!
        var request = URLRequest(url: url)
        request.httpMethod = target.method.rawValue
        
        if let headers = target.headers {
            for header in headers {
                request.setValue(header.value, forHTTPHeaderField: header.key)
            }
        }
        
        return request
    }
}
