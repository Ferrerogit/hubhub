//
//  GithubRepository.swift
//  hubhub
//
//  Created by Ярослав Попов on 26/05/2019.
//  Copyright © 2019 Ярослав Попов. All rights reserved.
//

import Foundation


struct GithubRepository: Decodable {
    
    enum CodingKeys: String, CodingKey {
        case forks
        case id
        case issues = "open_issues"
        case watchers
        case stars = "stargazers_count"
        case isPrivate = "private"
        case name
        case fullName = "full_name"
    }
    
    let fullName: String
    let forks: Int
    let id: Int
    let issues: Int
    let watchers: Int
    let stars: Int
    let isPrivate: Bool
    let name: String
}
