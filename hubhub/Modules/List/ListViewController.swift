//
//  ViewController.swift
//  hubhub
//
//  Created by Ярослав Попов on 26/05/2019.
//  Copyright © 2019 Ярослав Попов. All rights reserved.
//

import UIKit

class ListViewController: UITableViewController {
    
    let searchController = UISearchController(searchResultsController: nil)
    private var repositoriesList: [GithubRepository] = [GithubRepository]() {
        didSet {
            tableView.reloadData()
            refreshController.endRefreshing()
        }
    }
    
    private var searchBarDebounce: DispatchWorkItem?
    let refreshController = UIRefreshControl()
    
    var presenter: ListPresentation!

    override func viewDidLoad() {
        super.viewDidLoad()
        setup()
        presenter.viewDidLoad()
    }


    private func setup() {
        
        searchController.searchBar.placeholder = " Search repositories..."
        searchController.searchBar.sizeToFit()
        searchController.searchBar.isTranslucent = false
        searchController.searchBar.backgroundImage = UIImage()
        searchController.searchBar.barTintColor = .white
        navigationItem.title = "Search Repositories"
        searchController.searchBar.delegate = self
        searchController.delegate = self
        searchController.searchBar.tintColor = .white
        searchController.searchBar.barStyle = .black
        tableView.refreshControl = refreshController
        refreshController.addTarget(self, action: #selector(refresh(_:)), for: .valueChanged)
        
        self.searchController.dimsBackgroundDuringPresentation = false
        navigationItem.searchController = searchController
        navigationItem.hidesSearchBarWhenScrolling = false
        
        tableView.register(UINib(nibName: "RepositoryCell", bundle: nil), forCellReuseIdentifier: "repositoryCell")
        tableView.estimatedRowHeight = 96
        tableView.rowHeight = UITableView.automaticDimension
    }
    
    @objc private func refresh(_ sender: Any) {
        self.presenter.didBeginRefresh()
    }
    
    override func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return repositoriesList.count
    }
    
    override func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        presenter.didSelectCell(with: indexPath.row)
    }
    
    override func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        guard let cell = tableView.dequeueReusableCell(withIdentifier: "repositoryCell", for: indexPath) as? RepositoryCell else {
            return UITableViewCell()
        }
        
        cell.configure(repository: repositoriesList[indexPath.row])
        presenter.onLoadedCell(with: indexPath.row)
        return cell
    }
}

extension ListViewController: ListView {
    func open(repository: GithubRepository) {
        searchController.isActive = false
        let vc = UIStoryboard(name: "Main", bundle: nil).instantiateViewController(withIdentifier: String(describing: IssuesViewController.self)) as! IssuesViewController
        
        let presenter = IssuesPresenter(view: vc, repository: repository)
        
        vc.presenter = presenter
        self.navigationController?.pushViewController(vc, animated: true)
    }
    
    func set(repositoriesList: [GithubRepository]) {
        self.repositoriesList = repositoriesList
    }
}

extension ListViewController: UISearchControllerDelegate, UISearchBarDelegate, UISearchDisplayDelegate {
    
    func searchBar(_ searchBar: UISearchBar, textDidChange searchText: String) {
        searchBarDebounce?.cancel()
        searchBarDebounce = DispatchWorkItem {
            self.presenter.onSearchQueryChanged(newQuery: searchText)
        }
        DispatchQueue.main.asyncAfter(deadline: .now() + 1, execute: searchBarDebounce!)
    }

    func searchBar(_ searchBar: UISearchBar, shouldChangeTextIn range: NSRange, replacementText text: String) -> Bool {
        return text.isEmpty || text.matches("^[a-zA-Z0-9$@$!%*?&#^-_. +]+$")
    }
    
    func searchBarShouldBeginEditing(_ searchBar: UISearchBar) -> Bool {
        tableView.refreshControl = nil
        return true
    }
    
    func searchBarShouldEndEditing(_ searchBar: UISearchBar) -> Bool {
        tableView.refreshControl = refreshController
        return true
    }
}
