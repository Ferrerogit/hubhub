//
//  DataError.swift
//  hubhub
//
//  Created by Ярослав Попов on 26/05/2019.
//  Copyright © 2019 Ярослав Попов. All rights reserved.
//

import Foundation

enum DataError: Swift.Error, CustomStringConvertible {
    case noData
    case apiError
    
    var description: String {
        return ""
    }
}
