//
//  GithubIssueLabel.swift
//  hubhub
//
//  Created by Ярослав Попов on 27/05/2019.
//  Copyright © 2019 Ярослав Попов. All rights reserved.
//

import Foundation

struct GithubIssueLabel: Decodable {
    let name: String
    let color: String
}
